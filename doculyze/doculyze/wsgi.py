"""
WSGI config for doculyze project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.0/howto/deployment/wsgi/
"""

import os
from whitenoise import WhiteNoise
from django.core.wsgi import get_wsgi_application

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'doculyze.settings')
application = get_wsgi_application()
application = WhiteNoise(application, root=os.path.join(BASE_DIR, 'web/static/'))
#application.add_files('/path/to/more/static/files', prefix='more-files/')
